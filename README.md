# Stock updating example

I wrote this code when I worked as a tech support and salesperson in an online music store. My goal was to improve stock updating based on one of ours suppliers offer available in dedicated XML file. This XML file contains  only basic product info. The rest of important details like good quality photos, precise descriptions of products and their technical specifications I had to scrape from our supplier's website and put them into separate XML file.
The last task was to merge both these files into one with structure readable by tool delivered by our store service provider.

For these purposes I used, among others, following Python modules:
- [requests](https://pypi.org/project/requests/)
- [Beautiful Soup](https://pypi.org/project/beautifulsoup4/)
- [lxml](https://pypi.org/project/lxml/)

## App structure

## "wholesaler/main_files" folder
- **main_file.py** - runs all scripts
- **download_wholesaler.py** - downloads shared XML file
- **main_variables.py** - sets required paths, constants, global variables and urls
- **add_new_brand.py** - adds a new brand from this supplier and creates brand-named folder and files in "wholesaler/brands" folder
- **wholesaler.xml** - sample downloaded XML file

## "wholesaler/main_files/root_brand" folder
- templates used to create branded folders and files

## "wholesaler/main_files/xsl_4_all" folder
- **avail.xsl** - filters only available products
- **groups.xsl** - filters required group of products
- **names.xsl** - gets products names
- **price.xsl** - filters products at a given price

## "wholesaler/brands/example_brand/py_files" folder
- **main_example_brand_.py** - runs scripts for particular brand
- **variables_example_brand_.py** - sets required paths, constants and variables
- **createFiles_example_brand_.py** - filters required brand from supplier's XML file, and creates temporary XML files using XSLT stylesheets
- **getDetails_example_brand_.py** - scrapes details of particular brand from supplier's website and puts them into another XML file
- **transformXML_example_brand_.py** - combine both files into one ready to use by our store service provider tool
- **XSL_example_brand_.xsl** - XSLT stylesheet used by **crateFiles_example_brand_.py** file

## "wholesaler/brands/example_brand/tmp" folder
- temporary XML files used in transforming processes

## "wholesaler/brands/example_brand/example_brand_ready.xml" file
- **final result XML file** - feed for our online store service tool 
