import re
import shutil
import os
import main_variables as MV


def add_new_brand():
	new_brand = input('Write a name of brand, that you want to add: ')
	return new_brand


def check_brand(new_brand):
	check_directory = os.path.isdir(os.path.join(MV.APP_DIR, 'brands',
												new_brand))
	return check_directory


def make_brand_directory(new_brand):
	print(f'Creating \'{new_brand}\' directory...')
	os.mkdir(os.path.join(MV.APP_DIR, 'brands', new_brand))
	print(f'Directory \'{new_brand}\' with all files copied successfully!')


def close():
	print('Bye!')
	exit()


def typo(new_brand, x):
	if x == 0:
		print('What???! :)')
		rename_or_quit(new_brand)
	if x == 1:
		print('What???! :)')
		confirm(new_brand)


def rename_or_quit(new_brand):
	print('Do you want to add different brand, or do you want to quit?')
	answer = input('Type \'R\' if you want to rename, or \'Q\' if you want to '
			  'quit? ')
	if answer == 'R' or answer == 'r':
		main()
	elif answer == 'Q' or answer == 'q':
		close()
	else:
		typo(new_brand, x=0)


def confirm(new_brand):
	answer = input(f'Type \'Y\' if you want to add brand \'{new_brand}\', '
				   'or \'N\' if you do not: ')
	if answer == 'Y' or answer == 'y':
		make_brand_directory(new_brand)
		return new_brand
	elif answer == 'N' or answer == 'n':
		rename_or_quit(new_brand)
	else:
		typo(new_brand, x=1)


def prepare_paths(new_brand):
	src = os.path.join(MV.ROOT_DIR, 'root_brand', 'py_files')
	src_list = os.listdir(src)
	dst_path = os.path.join(MV.APP_DIR, 'brands', new_brand)
	dst = os.path.join(dst_path, 'py_files')
	os.mkdir(dst)
	os.mkdir(os.path.join(dst_path, 'tmp'))
	return src, src_list, dst


def copy_files(src, src_list, dst):
	new_files = []
	for file in src_list:
		file = os.path.join(src, file)
		new_file = shutil.copy(file, dst)
		new_files.append(new_file)
	print(new_files)
	return new_files

def change_mock(mock, f, new_brand):
	new_file = f.read()
	new_file = re.sub(mock, new_brand, new_file)
	f.seek(0)
	f.write(new_file)
	f.truncate()
	return f.name


def rename_files(new_files, new_brand, dst):
	mock = '_brand_'
	for file in new_files:
		with open(file, 'r+') as f:
			if '.xsl' in file:
				file = change_mock(mock, f, new_brand.upper())
			else:
				file = change_mock(mock, f, new_brand)
		file_name = os.path.split(file)[1]
		new_name = file_name.replace(mock, new_brand)
		os.rename(file, os.path.join(dst, new_name))


def main():
	new_brand = add_new_brand()
	check_directory = check_brand(new_brand)
	if not check_directory:
		new_brand = confirm(new_brand)
		(src, src_list, dst) = prepare_paths(new_brand)
		new_files = copy_files(src, src_list, dst)
		rename_files(new_files, new_brand, dst)

	else:
		print(f'Brand \'{new_brand}\' already exists!')
		rename_or_quit(new_brand)


main()
