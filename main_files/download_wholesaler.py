import requests

site_url = 'https://example_wholesaler.com/xml,12345'
o_file = 'wholesaler.xml'

s = requests.Session()

r = s.get(site_url)

with open(o_file, 'wb') as output:
    output.write(r.content)
print(f"File {o_file} downloaded successfully!")

s.close()
