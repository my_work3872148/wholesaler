<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
<products>&#xA;
<xsl:for-each select="/products/product[price_srp&gt;150]">
<xsl:copy-of select="."/>
</xsl:for-each>
</products>
</xsl:template>
</xsl:stylesheet>