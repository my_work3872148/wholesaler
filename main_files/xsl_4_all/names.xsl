<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="txt" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template match="/">

<xsl:for-each select="/products/product/name">
	<xsl:value-of select="."/>
	<xsl:text>&#xa;</xsl:text>
</xsl:for-each>
</xsl:template>

<xsl:template match = "/*/*/*[9]">
	<xsl:variable name = "subgroup" select = "local-name(following-sibling::*[1])" />
	<xsl:element name = "{local-name()}_{$subgroup}">
		<xsl:apply-templates select = "node()" />
		<xsl:text>,</xsl:text>
		<xsl:apply-templates select = "following-sibling::*[1]/node()" />
	</xsl:element>
</xsl:template>
<xsl:template match = "/*/*/*[position() > 9]" />
</xsl:stylesheet>