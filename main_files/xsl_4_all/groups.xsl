<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>
    
<xsl:template match = "/*/*/*[9]">
	<xsl:element name = "category">
		<xsl:apply-templates select = "node()" />
		<xsl:text>,</xsl:text>
		<xsl:apply-templates select = "following-sibling::*[1]/node()" />
	</xsl:element>
</xsl:template>
<xsl:template match = "/*/*/*[position() > 9]" />
</xsl:stylesheet>