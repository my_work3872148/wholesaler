import os.path
import requests
from bs4 import BeautifulSoup as BS
import lxml.etree as ET
import Wholesaler.main_files.main_variables as MV
import Wholesaler.brands._brand_.py_files.variables__brand_ as BV
from Wholesaler.brands._brand_.py_files.createFiles__brand_ import NAMES_LIST

brand_name = BV.BRAND_NAME
search_prefix = MV.SEARCH_PREFIX
main_url = MV.MAIN_URL
result_page = MV.RESULT_PAGE
next_page = MV.NEXT_PAGE
list_of_for_links_names = []
list_of_search_urls = []
products_details = []
end_result_list = []

# creating session object
session = requests.Session()

z = 1
for product_name in NAMES_LIST:
	# print(str(z) + '.  ' + product_name)
	z += 1
	# formatting name of product in the way that occure in direct link on this site
	if ' ' or '/' or '(' or ')' or '+' in product_name:
		for_link = product_name.replace('+', '-').replace(' ', '-') \
			.replace('/', '-').replace('(', '-').replace(')', '-') \
			.lower().strip()
		list_of_for_links_names.append(for_link)
		# list of product names formatted to search in soups

		# formatting name of product in the way that it occure in link
		# after writing it in search form
		for_search = product_name.replace('+', '%2B').replace(' ', '+')\
			.replace('/', '%2F').replace('(', '%28').replace(')', '%29').strip()
		# concatenating url of product after searching
		search_url = search_prefix + '&q=' + for_search
		# list of product names formatted to use for making soups
		list_of_search_urls.append(search_url)



# print(list_of_for_links_names)

# print(list_of_search_urls)

def get_product_stuff(product_name, product_link):
	# print(product_name)
	product_soup = BS(session.get(product_link).text, 'lxml')
	# print(product_link)
	# extract product details:
	# product description
	describe_tab = product_soup.find('div', {'id': 'zakP1'})
	description = describe_tab.getText().strip()
	if description:
		description = description.replace('\r', '')

	else:
		description = 'No description'
	describe_tab = description
	# print(describe_tab)

	# links related to product(events)
	links_tab = []
	div_with_links = product_soup.find('div', {'id': 'zakP2'})
	div_with_links = div_with_links.find_all('div', {'onclick': True})
	for link in div_with_links:
		direct_link = link.get('onclick').split("'")[1]
		if 'http' in direct_link:
			text_link = link.find('b').getText()
		else:
			direct_link = link.find('b').getText()
			text_link = link.get('onclick').split("'")[1]
		links_tab.append([direct_link])
	if not links_tab:
		links_tab = ''
	links_tab = links_tab
	# print(links_tab)

	# files attached to product(events)
	files_tab = []
	attach_files = product_soup.find('div', {'id': 'zakP3'})
	attach_files.find_all('div', {'onclick': True})
	for file in attach_files:
		file = file.strip()
		if 'No files for this product' in file or not file:
			files_tab = ''
		else:
			files_tab = 'There are some files!'
	files_tab = files_tab

	movie_tab = []
	movies_links = product_soup.find_all('iframe')
	mv_nr = 1
	if movies_links:
		for mv_link in movies_links:
			mv_link = 'https://youtu.be/' + (mv_link.get('src').split('/')[-1])
			if mv_link:
				if len(movies_links) > 1:
					movie_tab.append(
						[('Video presentation nr ' + str(mv_nr)), mv_link])
				else:
					movie_tab.append(['Video', mv_link])
		mv_nr += 1
	else:
		movie_tab = ''
	movie_tab = movie_tab
	# print(movie_tab)

	# get images of product
	images = []
	big_img = product_soup.find('div', {'id': 'Photo'}).find('img')
	image = big_img.get('src')
	images.append(image)

	div_with_images = product_soup.find('ul', {'id': 'Gallery'})\
		.find_all('a')
	for image in div_with_images:
		image = str(image.get('href'))
		images.append(image)
	# print(images)

	if big_img != 'ico/nophoto.gif' and describe_tab != 'No description':
		one_product = [product_name,
					   files_tab,
					   describe_tab,
					   movie_tab,
					   images]

		return one_product


def current_result_soup(url):
	result_soup = BS(session.get(url).text,'lxml')
	return result_soup


def find_product_url(result_soup):
	product_link = main_url + result_soup.find(
	'a', href=lambda x: x and link_name in x)['href']
	# print(product_link)
	return product_link


def find_next_page(result_soup, next_page):
	next_page_url = [a_tag['href'] for a_tag in result_soup
		.find_all('a') if a_tag.find('img', {'src' : next_page})][0]
	return next_page_url


def find_products_section(result_soup):
	products_section = result_soup.find('div', {'id' : 'Produkty'})
	products_section_text = products_section.text
	return products_section_text

def crawl_and_search(result_soup):
	next_page_url = find_next_page(result_soup, next_page)
	result_soup = current_result_soup(next_page_url)
	product_link = find_product_url(result_soup)
	if product_link:
		print(product_link)
		return product_link
	else:
		print('ups... :)')
		crawl_and_search(result_soup)


for product_name in NAMES_LIST:
	prod_index = NAMES_LIST.index(product_name)
	product_search_result_url =	list_of_search_urls[prod_index]
	link_name = brand_name + '-' + list_of_for_links_names[prod_index]
	result_soup = current_result_soup(product_search_result_url)
	products_section_text = find_products_section(result_soup)

	if 'No products.' in products_section_text:
		print('No products.')
	else:
		try:
			product_link = find_product_url(result_soup)
		except:
			product_link = crawl_and_search(result_soup)

		one_product = get_product_stuff(product_name, product_link)

		if one_product is not None:
			products_details.append(one_product)
		else:
			products_details = products_details

# print(products_details)

def make_xml(products_details):
	root_elem = ET.Element('products')
	root_elem = ET.SubElement(root_elem, 'products')

	for product in products_details:
		product_elem = ET.SubElement(root_elem, 'product')
		name_elem = ET.SubElement(product_elem, 'temp_name')
		files_elem = ET.SubElement(product_elem, 'files')
		description_elem = ET.SubElement(product_elem, 'description')
		videos_elem = ET.SubElement(product_elem, 'videos')
		images_elem = ET.SubElement(product_elem, 'images')

		name_elem.text = product[0]
		description_elem.text = product[2]
		for img in product[4]:
			image_elem = ET.SubElement(images_elem, 'image')
			image_elem.text = img
		root_elem.append(product_elem)

	tree = ET.ElementTree(root_elem)
	tree.write(os.path.join(BV.TMP_DIR, (brand_name + '_part1.xml')),
			   encoding='utf-8', xml_declaration=True,
			   pretty_print=True)

make_xml(products_details)
