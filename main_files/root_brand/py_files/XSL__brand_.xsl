<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
<products>&#xA;
<xsl:for-each select="/mainbase/products/product[brand = '_brand_']">
<product>&#xA;
	<id><xsl:value-of select="id"/></id>&#xA;
	<brand><xsl:value-of select="brand"/></brand>&#xA;
	<name><xsl:value-of select="code"/></name>&#xA;
	<avail><xsl:value-of select="inventory"/></avail>&#xA;
	<short_desc><xsl:value-of select="name"/></short_desc>&#xA;
	<price_srp><xsl:value-of select="price_srp"/></price_srp>&#xA;
	<price_orp><xsl:value-of select="price_orp"/></price_orp>&#xA;
	<price_dnp><xsl:value-of select="price_dnp"/></price_dnp>&#xA;
	<group><xsl:value-of select="group"/></group>&#xA;
	<subgroup><xsl:value-of select="subgroup"/></subgroup>
</product>&#xA;
</xsl:for-each>
</products>
</xsl:template>
</xsl:stylesheet>