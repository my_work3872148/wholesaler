import Wholesaler.main_files.main_variables as MV
import Wholesaler.brands._brand_.py_files.variables__brand_ as BV
import lxml.etree as ET
import os

brand_name = BV.BRAND_NAME
brand_directory = os.path.join(MV.BRANDS_DIR, brand_name)
parser = ET.XMLParser(remove_blank_text=True)

part1_xml = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_part1.xml')))
part2_xml = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_part2.xml')))
root_part1_xml = part1_xml.getroot()
root_part2_xml = part2_xml.getroot()

# lists of product names in both temporary xml files
list_part1 = root_part1_xml.findall('./*/temp_name')
list_part2 = root_part2_xml.findall('./product/name')

# Test if list_part2 contains all products from list_part1
# lists of text values of <name> tags from both temporary xml files
names_list_part1 = [name.text for name in list_part1]
names_list_part2 = [name.text for name in list_part2]

# from list_part2 remove products that do not match with products in list_part1
names_list_part2 = \
    [name for name in names_list_part2 if name in names_list_part1]

# check are list exact equal
if names_list_part1 == names_list_part2:
    print("all products from part1.xml ale present in part2.xml")
else:
    print('list of products scraped from webpage is not the same as list of '
          'products from xml file delivered by dealer')
target_products_list = []
for name in names_list_part1:
    target_product_tag = root_part1_xml.find('./product[temp_name = "{}"]'
                                            .format(name))
    data_fill_product_tag = root_part2_xml\
        .findall('./product[name = "{}"]/*'.format(name))
    for data_fill in data_fill_product_tag:
        target_product_tag.append(data_fill)
    target_products_list.append(target_product_tag)


def create_xml():
    products_root_element = ET.Element('products')
    products_root_element = ET.SubElement(products_root_element, 'products')
    for product in target_products_list:
        products_root_element.append(product)
    tree = ET.ElementTree(products_root_element)

    final_XML = os.path.join(brand_directory, brand_name + '_ready.xml')
    tree.write(final_XML, encoding='utf-8', xml_declaration=True,
                   pretty_print=True)


create_xml()
