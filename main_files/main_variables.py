import os

ROOT_DIR = os.path.dirname(__file__)
APP_DIR = os.path.realpath(os.path.join(ROOT_DIR, '..'))
BRANDS_DIR = os.path.join(APP_DIR, 'brands')
WHOLESALER_XML = os.path.join(ROOT_DIR, 'wholesaler.xml')

print(BRANDS_DIR)
print(WHOLESALER_XML)

xsl_directory = os.path.join(ROOT_DIR, 'xsl_4_all')
XSL_4_ALL = os.listdir(xsl_directory)

SEARCH_PREFIX = 'https://www.example_wholesaler.com/find,0,1.html?cf=1'
RESULT_PAGE = 'http://example_wholesaler.com/find,0,0,0,0,0,{},p.html?o=&sort='
MAIN_URL = 'https://www.example_wholesaler.com/'
NEXT_PAGE = '/gfx/right-arrow.gif'
