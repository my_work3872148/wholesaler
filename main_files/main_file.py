import os
import subprocess
import main_variables as MV

subprocess.run(['python', 'download_wholesaler.py'])


def create_paths(brand_name):
	brand_py_path = os.path.join(MV.BRANDS_DIR, brand_name, 'py_files')
	brand_main = ('main_' + brand_name + '.py')
	return brand_main, brand_py_path


def main():
	brands_list = os.listdir(MV.BRANDS_DIR)
	for brand_name in brands_list:
		(brand_main, brand_py_path) = create_paths(brand_name)
		subprocess.run(['python', brand_main], cwd=brand_py_path)
		print(f'Process completed. XML file for {brand_name} is ready to use.')


main()
