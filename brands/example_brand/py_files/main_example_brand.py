import subprocess

program_list = ['createFiles_example_brand.py', 'getDetails_example_brand.py',
                'transformXML_example_brand.py']

for program in program_list:
    subprocess.call(['python', program])
print("Process completed. File example_brand_ready.xml is ready to use.")