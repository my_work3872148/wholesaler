import os
from datetime import datetime
import lxml.etree as ET
import Wholesaler.main_files.main_variables as MV
import variables_example_brand as BV

# extract example_brand products from wholesaler.xml file
brand_name = BV.BRAND_NAME
dom = ET.parse(MV.WHOLESALER_XML)
xslt = ET.parse(BV.BRAND_XSL)
transform = ET.XSLT(xslt)
newdom = transform(dom)

with open(os.path.join(BV.TMP_DIR, (brand_name + '_main.xml')), 'wb') as main_xml:
	main_xml.write(bytes(newdom))
	now = datetime.now()
print(brand_name + ' products extracted from wholesaler.xml  file at ' + str(now))

# filter only available products
main_dom = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_main.xml')))
main_xslt = ET.parse(os.path.join(MV.xsl_directory, MV.XSL_4_ALL[0]))
transform = ET.XSLT(main_xslt)
main_newdom = transform(main_dom)
now = datetime.now()

with open(os.path.join(BV.TMP_DIR, (brand_name + '_available.xml')), 'wb') as available_xml:
	available_xml.write(bytes(main_newdom))
print(brand_name + '_available.xml file created at ' + str(now))

# filter products price offer 150 for client

avail_dom = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_available.xml')))
avail_xslt = ET.parse(os.path.join(MV.xsl_directory, MV.XSL_4_ALL[3]))
transform = ET.XSLT(avail_xslt)
newdom = transform(avail_dom)
now = datetime.now()
with open(os.path.join(BV.TMP_DIR, (brand_name + '_prices.xml')), 'wb') as prices_xml:
	prices_xml.write(newdom)
print(brand_name + '_prices.xml file created at ' + str(now))

# Create variable with list of products names - feed for "getDetails_" file
prices_dom = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_prices.xml')))
prices_xslt = ET.parse(os.path.join(MV.xsl_directory, MV.XSL_4_ALL[2]))
transform = ET.XSLT(prices_xslt)
newdom = transform(prices_dom)
names_list = str(newdom).split('\n')
NAMES_LIST = [name for name in names_list if name != '']
print(NAMES_LIST)

# Create file with joined group and subgroup elements prepared to merge with results of search.py file
category_dom = ET.parse(os.path.join(BV.TMP_DIR, (brand_name + '_prices.xml')))
category_xslt = ET.parse(os.path.join(MV.xsl_directory, MV.XSL_4_ALL[1]))
transform = ET.XSLT(category_xslt)
newdom = transform(category_dom)
now = datetime.now()
with open(os.path.join(BV.TMP_DIR, (brand_name + '_part2.xml')), 'wb') as part2:
	part2.write(newdom)
print(brand_name + '_part2.xml file created at ' + str(now))