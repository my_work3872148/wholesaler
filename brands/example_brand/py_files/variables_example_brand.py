import os
import Wholesaler.main_files.main_variables as MV


file_path = os.path.abspath(__file__)
file_parent_dir = os.path.abspath(os.path.join(file_path, os.pardir, os.pardir))
BRAND_NAME = os.path.split(file_parent_dir)[1]
BRAND_XSL = os.path.join(file_parent_dir, 'py_files',
						 ('XSL_' + BRAND_NAME + '.xsl'))
TMP_DIR = os.path.join(MV.BRANDS_DIR, BRAND_NAME, 'tmp')
